#!/usr/bin/env bash
set -euo pipefail

# number of nodes
NODE_COUNT=$(cat cluster.yaml | grep public_ip | wc -l)
echo "NODE_COUNT: ${NODE_COUNT}"

# nodes using nf_tables
IPTABLES_WITH_NF_TABLES=$(./hetznerctl foreach iptables --version | grep nf_tables | wc -l)
echo "IPTABLES_WITH_NF_TABLES: ${IPTABLES_WITH_NF_TABLES}"

if [[ "${NODE_COUNT}" == "${IPTABLES_WITH_NF_TABLES}" ]]; then
    echo "iptables uses nf_tables as a backend."
else
    echo "iptables does not use nf_tables as a backend."
    exit 1
fi

# flatcar
FLATCAR_IPTABLES_VERSION=$(./hetznerctl exec iptables --version)
echo "FLATCAR_IPTABLES_VERSION: ${FLATCAR_IPTABLES_VERSION}"

# kube-proxy
KUBE_PROXY_IPTABLES_VERSION=$(./hetznerctl exec kubectl -n kube-system exec ds/kube-proxy -- iptables --version)
echo "KUBE_PROXY_IPTABLES_VERSION: ${KUBE_PROXY_IPTABLES_VERSION}"

# kube-router
KUBE_ROUTER_IPTABLES_VERSION=$(./hetznerctl exec kubectl -n kube-system exec ds/kube-router -c kube-router -- iptables --version)
echo "KUBE_ROUTER_IPTABLES_VERSION: ${KUBE_ROUTER_IPTABLES_VERSION}"

# node-local-dns
NODE_LOCAL_DNS_IPTABLES_VERSION=$(./hetznerctl exec kubectl -n kube-system exec ds/node-local-dns -- iptables --version)
echo "NODE_LOCAL_DNS_IPTABLES_VERSION: ${NODE_LOCAL_DNS_IPTABLES_VERSION}"

# additional-metrics-exporter
ADDITIONAL_METRICS_IPTABLES_VERSION=$(./hetznerctl exec kubectl -n monitoring exec ds/additional-metrics-exporter -- iptables --version)
echo "ADDITIONAL_METRICS_IPTABLES_VERSION: ${ADDITIONAL_METRICS_IPTABLES_VERSION}"

# kube-proxy
if [[ "${FLATCAR_IPTABLES_VERSION}" != "${KUBE_PROXY_IPTABLES_VERSION}" ]]; then
    echo "iptables version of flatcar does not match the version of kube-proxy."
    exit 1
fi

# kube-router
if [[ "${FLATCAR_IPTABLES_VERSION}" != "${KUBE_ROUTER_IPTABLES_VERSION}" ]]; then
    echo "iptables version of flatcar does not match the version of kube-router."
    exit 1
fi

# node-local-dns
if [[ "${FLATCAR_IPTABLES_VERSION}" != "${NODE_LOCAL_DNS_IPTABLES_VERSION}" ]]; then
    echo "iptables version of flatcar does not match the version of node-local-dns."
    exit 1
fi

# additional-metrics-exporter
if [[ "${FLATCAR_IPTABLES_VERSION}" != "${ADDITIONAL_METRICS_IPTABLES_VERSION}" ]]; then
    echo "iptables version of flatcar does not match the version of additional-metrics-exporter."
    exit 1
fi

# duplicate rules
./hetznerctl foreach "iptables-save | grep KUBE | sort | uniq -c | sort | tail -n 1 | awk '\$1 > 4 {print; err = 1} END {exit err}'"