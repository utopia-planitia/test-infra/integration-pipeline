#!/usr/bin/env bash
set -exuo pipefail

# release flatcar image in nebraska
APP_ID=`cat beta.json | jq -r '.application_id'`
echo "APP_ID ${APP_ID}"

PACKAGE_ID=`cat beta.json | jq -r '.package_id'`
echo "PACKAGE_ID ${PACKAGE_ID}"

STABLE_CHANNEL_JSON=`curl --request GET --fail --max-time 10 \
  --url https://nebraska.flatcar.turbinekreuzberg.io/api/apps/io.turbinekreuzberg.linux/channels \
  -u "${NEBRASKA_USERNAME}:${NEBRASKA_PASSWORD}" | jq '.channels[] | select(.name == "stable")'`
echo "STABLE_CHANNEL_JSON ${STABLE_CHANNEL_JSON}"

STABLE_CHANNEL_ID=`echo "$STABLE_CHANNEL_JSON" | jq -r '.id'`
echo "STABLE_CHANNEL_ID ${STABLE_CHANNEL_ID}"

STABLE_PACKAGE_VERSION=`echo "$STABLE_CHANNEL_JSON" | jq -r '.package.version'`
echo "STABLE_PACKAGE_VERSION ${STABLE_PACKAGE_VERSION}"

PACKAGE_VERSION=`cat beta.json | jq -r '.package.version'`
echo "PACKAGE_VERSION ${PACKAGE_VERSION}"

if [[ "$STABLE_PACKAGE_VERSION" == "$PACKAGE_VERSION" ]]; then
  echo "skip release: nothing has changed"
	exit 0
fi

mc alias set flatcar "${FLATCAR_MINIO_URL}" "${FLATCAR_MINIO_USERNAME}" "${FLATCAR_MINIO_PASSWORD}"
mc cp flatcar/flatcar-images/${PACKAGE_VERSION}/version.txt flatcar/flatcar-images/stable/current/version.txt

curl --request PUT --silent --fail --max-time 5 \
  --url "https://nebraska.flatcar.turbinekreuzberg.io/api/apps/io.turbinekreuzberg.linux/channels/${STABLE_CHANNEL_ID}" \
  -u "${NEBRASKA_USERNAME}:${NEBRASKA_PASSWORD}" \
  --header 'Content-Type: application/json' \
  --data "{
    \"application_id\": \"${APP_ID}\",
    \"arch\": 1,
    \"package_id\": \"${PACKAGE_ID}\",
    \"name\": \"stable\",
    \"color\": \"#c62828\"
  }"
