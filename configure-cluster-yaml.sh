#!/usr/bin/env bash
set -exuo pipefail

echo ${CLUSTER_YAML} | base64 -d > cluster.yaml

curl --request GET --fail --max-time 10 \
  --url ${NEBRASKA_URL}/api/apps/${NEBRASKA_PRODUCT_ID}/channels \
  -u "${NEBRASKA_USERNAME}:${NEBRASKA_PASSWORD}" | jq '.channels[] | select(.name == "beta")' \
  > beta.json

PACKAGE_VERSION=`cat beta.json | jq -r '.package.version'`
echo "PACKAGE_VERSION ${PACKAGE_VERSION}"

yq -i e ".flatcar.version = \"${PACKAGE_VERSION}\"" cluster.yaml
yq -i e ".flatcar.disable_updates = true" cluster.yaml
