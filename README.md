# Integration Pipeline

This repo validates the interactions between [hetznerctl](https://gitlab.com/utopia-planitia/hetznerctl) and [utopia planitia](https://gitlab.com/utopia-planitia)s preconfigured CI services.

Hetznerctl deploys a operating system, installs [kubernetes](https://github.com/kubernetes/kubernetes/) via [kubeadm](https://kubernetes.io/docs/reference/setup-tools/kubeadm/), and deploys the services via [helmfile](https://github.com/roboll/helmfile).

Services containing tests get verified via [bats](https://github.com/bats-core/bats-core) tests. Kubernetes gets validated in addition by running the official [end to end tests](https://github.com/kubernetes/kubernetes/tree/master/test/e2e).

Once everything is validated, a branch `verified` is pointed at the current commit.
